<style type="text/css">
body{
    background-image:url("<?php echo URL_ASSETS ?>imagenes/index.jpg");
    background-size:cover;
    background-attachment: fixed;
    color: #000;
    }
    p{
      color: red;
      margin: 0;
    }

    </style>
<div class="container">
    <div class="col-md-12 col-md-offset-0">  
        <div>
            <h2  class="well well-sm" style="background-color: #000; text-align: center; border: none; "><font face="Cooper Black" color="#fff">Especies</font></h2>
        </div>
        
        <div class="well well-sm text-right">
            <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal"><span class="glyphicon glyphicon-edit"> </span> Registrar nueva especie </button>
            <a href="?c=Index&m=indexA" ><button type="button" class="btn btn-danger btn-lg">Cancelar <span class="glyphicon glyphicon-remove"> </span></button></a>
        </div>

        <?php include (URL_MODAL. 'modal_especie.php'); ?>

       	<div class="panel-body">
		   	<table class="table table-hover table-striped">
		   		<tr>
		   			<th>Especies</th>
		   			<th>Modificar</th>
		   			<th>Eliminar</th>
		   		</tr>
	       	<?php 
	       	foreach ($this->especie->getEspecie() as $result ) {
	       	?>
	       	<tr>
                     	
                <td><?php echo $result->nomEspecie; ?></td>
                <td><a href="?c=Index&m=crud" <?php echo $result->idEspecie; ?>  class="btn btn-success" ><span class="glyphicon glyphicon-pencil"></span></a></td>
                <td><a onclick="javascript:return confirm('¿Seguro de eliminar este registro?');" class="btn btn-warning" href="?c=Index&m=eliminarEspecie&idEspecie=<?php echo $result->idEspecie; ?>"><span class="glyphicon glyphicon-trash" aria-hidden="true"> </span></a>
                </td>
            </tr>

	       	<?php  
	       	}
	       	?> 
       </div>
    </div>
</div>       