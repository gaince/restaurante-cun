<style type="text/css">

  p{
    margin: 0;
    color: red;
    padding-bottom: 12px;
}

    body{
    background-image:url("<?php echo URL_ASSETS ?>imagenes/fondo.jpg");
    background-size: 100% 100%;
    background-attachment: fixed;
   color: #000;
    }

  }</style>
<div class="container">
  <form class="well form-horizontal" action="?c=Index&m=registrarUsuario" method="POST"   id="contacto-frm" name="contacto_frm">
  
    <fieldset>
    <!-- Formulario Registro -->
    <center>
    <legend><FONT SIZE="10" color="#000000" face="Cooper Black">Registro</FONT></legend></center>
    <!-- Text input-->

    <div class="form-group">
      <label class="col-md-4 control-label">Nombres</label>  
      <div class="col-md-4 inputGroupContainer">
        <div class="input-group">
          <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
          <input  type="text" class="form-control" placeholder="Nombre completo" name="nombre"  id="nombre" onkeyup="validarNombre();">
        </div><p id="nombrer"></p>
      </div>
    </div>
    <!-- Text input-->

    <div class="form-group">
      <label class="col-md-4 control-label" >Nombre de Usuario</label> 
        <div class="col-md-4 inputGroupContainer">
          <div class="input-group">
            <span class="input-group-addon help-block"><i class="glyphicon glyphicon-user"></i></span>
            <input type="text" class="form-control" placeholder="Usuario"  name="usuario" id="usuario" onkeyup="validarUsuario();"> 
        </div><p id="usuarior"></p>
      </div>
    </div>

    <!-- Text input Contraseña-->
    <div class="form-group">
      <label class="col-md-4 control-label">Contraseña</label>  
       <div class="col-md-4 inputGroupContainer">
        <div class="input-group">
            <span class="input-group-addon"><i class="glyphicon glyphicon-globe"></i></span>
            <input type="password" class="form-control" placeholder="Password" name="clave" id="clave" onkeyup="contrasena();">
        </div><p id="claver"></p>
      </div>
    </div>

    <!-- Text input Repita Contraseña-->
    <div class="form-group">
      <label class="col-md-4 control-label"> Repita Contraseña</label>  
       <div class="col-md-4 inputGroupContainer">
        <div class="input-group">
            <span class="input-group-addon"><i class="glyphicon glyphicon-globe"></i></span>
            <input type="password" class="form-control" placeholder="Password" name="clave1" id="reClave" onkeyup="contrasena();">
        </div><p id="reClaver"></p>
      </div>
    </div>

<!-- Select documento -->       
<div class="form-group"> 
      <label class="col-md-4 control-label">Tipo documento</label>
        <div class="col-md-4 selectContainer">
          <div class="input-group">
            <span class="input-group-addon"><i class="glyphicon glyphicon-list"></i></span>
              <select name="documento" class="form-control" id="documento" onchange="validarDocumento();">
                  <option value="0">Seleccione un tipo de documento</option>
                    <?php 
                    foreach ($this->model->getTipoDocumento() as $result) {
                       echo"<option value=\"".$result->documento."\">"
                        .$result->documento."</option>";
                    }
                     ?>
              </select>
          </div><p id="documentor"></p>
        </div>
    </div>
    <div class="form-group">
      <label class="col-md-4 control-label" >Numero</label> 
        <div class="col-md-4 inputGroupContainer">
          <div class="input-group">
            <span class="input-group-addon help-block"><i class="glyphicon glyphicon-user"></i></span>
            <input type="text" class="form-control" placeholder="numero del documento"  name="numero" id="numero" onkeyup="validarNumero();"> 
        </div><p id="numeror"></p>
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-4 control-label" >Comisión</label> 
        <div class="col-md-4 inputGroupContainer">
          <div class="input-group">
            <span class="input-group-addon help-block"><i class="glyphicon glyphicon-user"></i></span>
            <input type="text" class="form-control" placeholder="Comisión"  name="comision" id="comision" onkeyup="validarNumero();"> 
        </div><p id="comisionr"></p>
      </div>
    </div>
    <!-- Select Rol -->       
    <div class="form-group"> 
      <label class="col-md-4 control-label">Rol</label>
        <div class="col-md-4 selectContainer">
          <div class="input-group">
            <span class="input-group-addon"><i class="glyphicon glyphicon-list"></i></span>
              <select name="rol" class="form-control" id="rol" onchange="validarRol();">
                  <option value="0">Seleccione un rol</option>
                    <?php 
                    foreach ($this->model->getRol() as $result) {
                       echo"<option value=\"".$result->nombreRol."\">"
                        .$result->nombreRol."</option>";
                    }
                     ?>
              </select>
          </div><p id="rolr"></p>
        </div>
    </div>
    <!-- Button -->
    <div class="form-group">
       <label class="col-md-4 control-label"></label>
      <div class="col-md-4">
        <button type="button" class="btn btn-info" name="bEnviar" >Registrarse <span class="glyphicon glyphicon-send"> </span></button>
        <a href="?c=Index&m=indexV"><button type="button" class="btn btn-danger">Cancelar <span class="glyphicon glyphicon-remove"> </span></button></a>
      </div>
    </div>

    </fieldset>
  </form>
</div>