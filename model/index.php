<?php 
class index{
     private $pdo;
	
	public function __construct(){
		try {
			$this->pdo=database::Conectar();
		} catch (Exception $e) {
			
		}
	}

	public function inicio($usuario){
		try { 
		$stm=$this->pdo->prepare("SELECT * FROM persona 
		inner join rol on(persona.rol = rol.nombreRol)
		WHERE persona.usuario= '$usuario' and persona.rol=rol.nombreRol");
		$stm->execute();
		return $stm->fetchAll(PDO::FETCH_OBJ);
		
		} catch (Exception $e) {
			die($e->getMessage());
		}
	}

	public function getRol(){
		
		try {
			$stm=$this->pdo->prepare("SELECT * from rol");
			$stm->execute();
			return $stm->fetchAll(PDO::FETCH_OBJ);
		} catch (Exception $e) {
			die($e->getMessage());
		}
	}

	public function saveRol($nombreRol){
		try { 
			$stm=$this->pdo->prepare("INSERT into rol values('$nombreRol')");
			$stm->execute();

		} catch (Exception $e) {
			die($e->getMessage());
		}
	}

	public function deleteRol($nombreRol)
	{
		try 
		{
			$stm = $this->pdo->prepare("DELETE FROM rol WHERE nombreRol = '$nombreRol'");
			$stm->execute();			          
		} catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}

	// funcionalidad crud tipo documento

	public function getTipoDocumento(){
		
		try {
			$stm=$this->pdo->prepare("SELECT * from tipo_documento");
			$stm->execute();
			return $stm->fetchAll(PDO::FETCH_OBJ);
		} catch (Exception $e) {
			die($e->getMessage());
		}
	}

	public function saveTipoDocumento($tipoDocumento){
		try { 
			$stm=$this->pdo->prepare("INSERT into tipo_documento values('$tipoDocumento')");
			$stm->execute();

		} catch (Exception $e) {
			die($e->getMessage());
		}
	}
	
	public function deleteTipoDocumento($documento)
	{
		try 
		{
			$stm = $this->pdo->prepare("DELETE FROM tipo_documento WHERE documento = '$documento'");
			$stm->execute();			          
		} catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}

	public function getByIdTipoDocumento($documento){
    	try 
		{
			$stm = $this->pdo->prepare("SELECT * FROM tipo_documento WHERE documento = '$documento'");
			$stm->execute();

			return $stm->fetchAll(PDO::FETCH_OBJ);
		} catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}


	public function getPerson(){
		
		try {
			$stm=$this->pdo->prepare("SELECT * from persona");
			$stm->execute();
			return $stm->fetchAll(PDO::FETCH_OBJ);
		} catch (Exception $e) {
			die($e->getMessage());
		}
	}

	public function registrarUsuarios($nombreUsu,$usuario,$clave, $documento, $numero, $comision, $rol){
		try {
			$encriptada =password_hash($clave,PASSWORD_BCRYPT);

			$stm=$this->pdo->prepare("INSERT into persona (nombre,usuario,clave,documento,numero, comision,rol) 
				values('$nombreUsu','$usuario','$encriptada','$documento', '$numero', '$comision','$rol')");
			$stm->execute();

		} catch (Exception $e) {
			die($e->getMessage());
		}
	}

}

 ?>