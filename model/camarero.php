<?php 
class Camareros{
	private $pdo;
	
	public function __construct(){
		try {
			$this->pdo=database::Conectar();
			
		} catch (Exception $e) {
			die("error");
		}
	}

	public function getCamareros(){
			try {
				
				$stm=$this->pdo->prepare(" SELECT * FROM camareros");
				$stm->execute();
				
				return $stm->fetchAll(PDO::FETCH_OBJ);

			} catch (Exception $e) {
				die($e->getMessage());
			}
    }

    public function registrarCamareros($nombre, $comision){
    	try {
    		$stm=$this->pdo->prepare("INSERT into camareros (nombre, comision) values('$nombre', '$comision')");
    		$stm->execute();
    	} catch (Exception $e) {
    		die($e->getMessage());
    	}
    }

    public function obtenerCamareros($idCamarero){
    	try 
		{
			$stm = $this->pdo->prepare("SELECT * FROM camareros WHERE id = '$idCamarero'");
			$stm->execute();

			return $stm->fetchAll(PDO::FETCH_OBJ);
		} catch (Exception $e) 
		{
			die($e->getMessage());
		}
    }

	public function eliminarCamarero($idCamarero)
	{
		try 
		{
			$stm = $this->pdo->prepare("DELETE FROM camareros WHERE id = '$idCamarero'");
			$stm->execute();			          
		} catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}

	public function editarCamarero($idCamarero,$nombre){
		try {
			$stm=$this->pdo->prepare("UPDATE camareros set nombre='$nombre' where id='$idCamarero'");
			$stm->execute();

		} catch (Exception $e) {
			die($e->getMessage());
		}
		
	}
}
?>